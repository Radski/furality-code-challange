import { fireEvent, render } from "@testing-library/react";
import Form from "../pages/form";
import "@testing-library/jest-dom";
import { act } from "react-dom/test-utils";

describe("Form", () => {
  it("shound render the form", () => {
    const { getByLabelText, getByDisplayValue } = render(<Form />);

    const emailField = getByLabelText("email");
    const radioOptionOne = getByLabelText("Cheese sandwich");
    const radioOptionTwo = getByLabelText("Glass of milk");
    const submit = getByDisplayValue("submit");

    expect(radioOptionOne).toBeVisible();
    expect(radioOptionTwo).toBeVisible();
    expect(submit).toBeVisible();
    expect(emailField).toBeVisible();
  });

  it("should have only one radio button checked when clicked", () => {
    const { getByLabelText, getByDisplayValue } = render(<Form />);
    const radioOptionOne = getByLabelText("Cheese sandwich");
    const radioOptionTwo = getByLabelText("Glass of milk");

    fireEvent.click(radioOptionOne);
    expect(radioOptionOne).toBeChecked();
    expect(radioOptionTwo).not.toBeChecked();
  });

  it("should not validate with an incorrect email address", async () => {
    const { getByLabelText, getByDisplayValue } = render(<Form />);
    const emailField = getByLabelText("email");
    const radioOptionOne = getByLabelText("Cheese sandwich");
    const submit = getByDisplayValue("submit");

    await act(async () => {
      fireEvent.change(emailField, { target: { value: "user.owo" } });
      fireEvent.click(radioOptionOne);
      fireEvent.click(submit);
    });
    expect(emailField.checkValidity()).not.toBeTruthy();
  });
});
