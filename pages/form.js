import Head from "next/head";
import styles from "../styles/Form.module.scss";

const handleSubmit = async (e) => {
  e.preventDefault();
  const data = {
    email: e.target.email.value,
    choice: e.target.choice.value,
  };

  // Handle submit
  // Normally I would make a class for this to add authentication headers. Quite unnecessary for this one request.
  try {
    const resp = await fetch("api/endpoint", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });

    if (resp.ok) {
      console.log(await resp.json());
    }
  } catch (err) {
    console.error(err);
  }
};

const Form = () => {
  return (
    <div className={styles.formContainer}>
      <Head>
        <title>Form</title>
        <meta property="og:title" content="Form" />
      </Head>
      <h1>Lorem ipsum</h1>
      <form className={styles.form} onSubmit={handleSubmit}>
        <h2>Questions</h2>
        <div className={styles.textInputContainer}>
          <label htmlFor="email" className={styles.textInputLabel}>
            email
          </label>
          <input
            type="email"
            name="email"
            id="email"
            className={styles.textInput}
            placeholder="name@site.com"
            required
          />
        </div>
        <div className={styles.radioContainer}>
          <p>
            <strong>What do you prefer?</strong>
          </p>

          <div className={styles.radioItem}>
            <input
              className={styles.radioButton}
              required
              type="radio"
              name="choice"
              id="cheese"
              value="cheese"
            />
            <label className={styles.radioLabel} htmlFor="cheese">
              Cheese sandwich
            </label>
          </div>

          <div className={styles.radioItem}>
            <input
              className={styles.radioButton}
              required
              type="radio"
              name="choice"
              id="milk"
              value="milk"
            />
            <label className={styles.radioLabel} htmlFor="milk">
              Glass of milk
            </label>
          </div>
        </div>
        <input type="submit" className={styles.submitButton} value="submit" />
      </form>
    </div>
  );
};

export default Form;
