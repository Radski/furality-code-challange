import Head from "next/head";
import Image from "next/image";
import styles from "../styles/Image.module.scss";

const ImagePage = () => {
  return (
    <div className={styles.container}>
      <Head>
        <title>Image</title>
        <meta property="og:title" content="Image" />
      </Head>
      <div className={styles.imageContainer}>
        <h1 className={styles.heading}>Lorem Ipsum</h1>
        <Image
          src="/furret.jpg"
          width={128}
          height={128}
          className={styles.image}
          alt="Furret"
        />
      </div>
    </div>
  );
};

export default ImagePage;
