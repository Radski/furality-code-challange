import Head from "next/head";
import styles from "../styles/Index.module.scss";

const Index = () => {
  return (
    <div className={styles.container}>
      <Head>
        <title>Index</title>
        <meta property="og:title" content="Coding challange for furality" />
      </Head>
      <h1>Radski</h1>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
        commodo consequat.
      </p>
    </div>
  );
};

export default Index;
