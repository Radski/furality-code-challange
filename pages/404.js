import Head from "next/head";
import styles from "../styles/404.module.scss";

const FourOFour = () => {
  return (
    <div className={styles.container}>
      <Head>
        <title>404 Not found</title>
      </Head>
      <h1>ÒwÓ</h1>
      <h2>page could not be found</h2>
      <p>404</p>
    </div>
  );
};

export default FourOFour;
