import { useRouter } from "next/router";
import Link from "next/link";

const ActiveLink = ({ children, href, activeClassName, basicClass }) => {
  const router = useRouter();

  const cssClass = `${basicClass} ${
    router.asPath === href ? activeClassName : ""
  }`;

  return (
    <Link href={href} className={cssClass} draggable={false}>
      {children}
    </Link>
  );
};

export default ActiveLink;
