import styles from "../styles/Navbar.module.scss";
import ActiveLink from "./ActiveLink";

const Navbar = () => {
  return (
    <div className={styles.navbar}>
      <h1>Website</h1>
      <ul className={styles.list}>
        <li>
          <ActiveLink
            basicClass={styles.listItem}
            activeClassName={styles.active}
            href={"/"}
          >
            index
          </ActiveLink>
        </li>
        <li>
          <ActiveLink
            basicClass={styles.listItem}
            activeClassName={styles.active}
            href={"/image"}
          >
            image
          </ActiveLink>
        </li>
        <li>
          <ActiveLink
            basicClass={styles.listItem}
            activeClassName={styles.active}
            href={"/form"}
          >
            form
          </ActiveLink>
        </li>
      </ul>
    </div>
  );
};

export default Navbar;
