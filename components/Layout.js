import Navbar from "./Navbar";

const Layout = ({ children }) => {
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        width: '100vw',
        height: '100vh',
      }}
    >
      <Navbar />
      <main>{children}</main>
    </div>
  );
};

export default Layout;
